User.create!([
  {first_name: "Josh", last_name: "Nankin"},
  {first_name: "Howard", last_name: "Avner"}
])
UserPhoneNumber.create!([
  {phone_number: "+14145341207", user_id: 1},
  {phone_number: "+18474149438", user_id: 2}
])
