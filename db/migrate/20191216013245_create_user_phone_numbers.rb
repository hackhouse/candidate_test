class CreateUserPhoneNumbers < ActiveRecord::Migration[5.1]
  def change
    create_table :user_phone_numbers do |t|
      t.string :phone_number
      t.references :user

      t.timestamps
    end
  end
end
