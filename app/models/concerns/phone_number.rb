module PhoneNumber
  extend Phonelib::Core

  def self.parse *args
    # If first arg is not e164 and contains exactly 10 digits, assume NANP
    args[0] = args[0].to_s
    orig_num = args[0]
    if args[0].strip[0] != '+' and args[0].count('0-9').eql?(10)
      args[0] = "+1" + args[0]
    end

    num = Number.new *args
    num.instance_variable_set :@original, orig_num
    num
  end

  def self.e164! string
    self.parse(string).e164
  end

  class Number < Phonelib::Phone
    def national_plain
      @national_number
    end

    def toll_free?
      type == :toll_free
    end

    def to_s
      e164
    end
  end
end
