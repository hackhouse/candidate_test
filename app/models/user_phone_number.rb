class UserPhoneNumber < ApplicationRecord
  belongs_to :user, required: true
  before_save :save_phone_numbers_as_e164
  validate do
    errors.add(:phone_number, 'Not a valid phone number') unless PhoneNumber.parse(phone_number).valid?
  end

  def save_phone_numbers_as_e164
    self.phone_number = PhoneNumber.parse(phone_number).e164
  end
end
